const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
const config = require("config");
const JWTSecret = config.get("jwtsecter");

const Load = require("../models/load");
const Truck = require("../models/truck");
const User = require("../models/user");
const truckTypes = require("../truckTypes");

const generateAccessToken = (id) => {
  const payload = {
    id,
  };
  return jwt.sign(payload, JWTSecret, { expiresIn: "24h" });
};

class AuthController {
  async registration(req, res) {
    try {
      const err = validationResult(req);
      if (!err.isEmpty()) {
        console.log(err);
        return res.status(400).send({ message: "Error message isE" });
      }
      const { email, password, role } = req.body;
      const candidate = await User.findOne({ email });
      if (candidate) {
        return res.status(400).send({ message: "Error message candidate" });
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const user = new User({
        email,
        password: hashPassword,
        role,
        createdDate: new Date(),
      });
      await user.save();
      return res.status(200).send({ message: "Success" });
    } catch (e) {
      console.log(e);
      res.status(400).send({ message: "Error message catch" });
    }
  }

  async login(req, res) {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(400).send({ message: "Error message" });
      }

      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).send({ message: "Error message" });
      }
      const token = generateAccessToken(user._id);
      return res.status(200).send({
        jwt_token: token,
      });
    } catch (e) {
      console.log(e);
      res.status(400).send({ message: "Error message" });
    }
  }

  async getUser(req, res) {
    try {
      const user = await User.findById(req.user.id);
      const result = {
        user: {
          _id: user.id,
          role: user.role,
          email: user.email,
          createdDate: user.createdDate,
        },
      };
      return res.status(200).send(result);
    } catch (e) {
      res.status(400).send({ message: "Error message" });
      throw e;
    }
  }

  async deleteUser(req, res) {
    try {
      await User.findByIdAndRemove(req.user.id);
      return res.status(200).send({
        message: "Success",
      });
    } catch (e) {
      res.status(400).send({ message: "Error message" });
      throw e;
    }
  }

  async changePassword(req, res) {
    try {
      const user = await User.findOne({ _id: req.user.id });
      const validOldPassword = await bcrypt.compare(
        req.body.oldPassword,
        user.password
      );
      if (!validOldPassword) {
        res.status(400).send({ message: "Error message" });
      } else {
        const salt = await bcrypt.genSalt(7);
        const hashPassword = await bcrypt.hash(req.body.newPassword, salt);
        await User.findOneAndUpdate(
          { _id: req.user.id },
          { password: hashPassword }
        );
        return res.status(200).send({ message: "Success" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async getTrucks(req, res) {
    try {
      const allTrucks = await Truck.find({ created_by: req.user.id }).select(
        "-__v"
      );
      return res.status(200).send({
        trucks: allTrucks,
      });
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async postTruck(req, res) {
    try {
      const truck = new Truck({
        created_by: req.user.id,
        assigned_to: null,
        type: req.body.type,
        status: "IS",
        created_date: new Date(),
      });
      await truck.save();
      console.log("Truck created");

      return res.status(200).send({ message: "Truck created successfully" });
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async getTruckById(req, res) {
    try {
      const truck = await Truck.findOne({ _id: req.params.id }).select("-__v");
      if (truck) {
        console.log("Get truck by id success");
        return res.status(200).send({ truck: truck });
      } else {
        console.log("No truck with this id");
        return res.status(400).send({ message: "Error message" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async updateTruckById(req, res) {
    try {
      if (
        await Truck.findOneAndUpdate(
          { _id: req.params.id },
          { type: req.body.type }
        )
      ) {
        console.log("Truck update success");
        return res
          .status(200)
          .send({ message: "Truck details changed successfully" });
      } else {
        console.log("No truck with this id");
        return res.status(400).send({ message: "Error message" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async deleteTruckById(req, res) {
    try {
      if (await Truck.findOneAndDelete({ _id: req.params.id })) {
        console.log("Truck deleted success");
        return res.status(200).send({ message: "Truck deleted successfully" });
      } else {
        console.log("No truck with this id");
        return res.status(400).send({ message: "Error message" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async assignTruckToUser(req, res) {
    try {
      if ((await Truck.findOne({ assigned_to: req.user.id })) === null) {
        if (
          await Truck.findOneAndUpdate(
            { _id: req.params.id },
            { assigned_to: req.user.id }
          )
        ) {
          console.log("Truck assigned success");
          return res
            .status(200)
            .send({ message: "Truck assigned successfully" });
        } else {
          console.log("No truck with this id");
          return res.status(400).send({ message: "Error message" });
        }
      } else {
        console.log("User is already assigned");
        return res.status(400).send({ message: "Error message" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async getLoads(req, res) {
    try {
      const user = await User.findOne({ _id: req.user.id });
      let limit = 10;
      if (req.query.limit) {
        limit = req.query.limit > 50 ? 50 : +req.query.limit;
      }

      if (user.role === "DRIVER") {
        await Load.find({ assigned_to: req.user.id })
          .skip(+req.query.offset || 0)
          .limit(limit)
          .select("-__v")
          .then((allLoads) => {
            return res.status(200).send({ loads: allLoads });
          })
          .catch((error) => {
            console.error(error);
            res.status(400).send({ message: "Error message" });
          });
      } else {
        await Load.find({ created_by: req.user.id })
          .skip(+req.query.offset || 0)
          .limit(limit)
          .select("-__v")
          .then((allLoads) => {
            return res.status(200).send({ loads: allLoads });
          })
          .catch((error) => {
            console.error(error);
            res.status(400).send({ message: "Error message" });
          });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async addLoad(req, res) {
    try {
      const load = new Load({
        name: req.body.name,
        payload: req.body.payload,
        pickup_address: req.body.pickup_address,
        delivery_address: req.body.delivery_address,
        created_by: req.user.id,
        logs: [],
        assigned_to: null,
        status: "NEW",
        state: "",
        dimensions: req.body.dimensions,
        created_date: new Date(),
      });
      await load.save();
      console.log("Load created");

      return res.status(200).send({ message: "Load created successfully" });
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async getActiveLoad(req, res) {
    try {
      await Load.findOne({ assigned_to: req.user.id, status: "ASSIGNED" })
        .select("-__v")
        .then((load) => {
          console.log("Load find success");
          return res.status(200).send({ load: load });
        })
        .catch((error) => {
          console.log("This drive have not load");
          res.status(400).send({ message: "Error message" });
        });
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async iterateToNextLoadState(req, res) {
    let newState = "";
    try {
      await Load.findOne({ assigned_to: req.user.id, status: "ASSIGNED" }).then(
        (load) => {
          if (load.state === "En route to Pick Up") {
            newState = "Arrived to Pick Up";
            load.state = "Arrived to Pick Up";
          } else if (load.state === "Arrived to Pick Up") {
            newState = "En route to delivery";
            load.state = "En route to delivery";
          } else {
            newState = "Arrived to delivery";
            load.state = "Arrived to delivery";
            load.status = "SHIPPED";
          }
          load.save();
        }
      );
      if (newState === "Arrived to delivery") {
        await Truck.findOneAndUpdate(
          { assigned_to: req.user.id },
          { status: "IS" }
        );
      }

      return res
        .status(200)
        .send({ message: "Load state changed to 'En route to Delivery'" });
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async getLoadById(req, res) {
    try {
      await Load.findOne({ _id: req.params.id })
        .select("-__v")
        .then((load) => {
          console.log("Load get success");
          return res.status(200).send({ load: load });
        })
        .catch((error) => {
          console.log("No load with this id");
          return res.status(400).send({ message: "Error message" });
        });
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async updateLoadById(req, res) {
    try {
      if (
        await Load.findByIdAndUpdate(
          { _id: req.params.id },
          {
            name: req.body.name,
            payload: req.body.payload,
            pickup_address: req.body.pickup_address,
            delivery_address: req.body.delivery_address,
            dimensions: req.body.dimensions,
          }
        )
      ) {
        console.log("Load update success");
        return res
          .status(200)
          .send({ message: "Load details changed successfully" });
      } else {
        console.log("No load with this id");
        return res.status(400).send({ message: "Error message" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async deleteLoadById(req, res) {
    try {
      if (await Load.findOneAndDelete({ _id: req.params.id })) {
        console.log("Load deleted success");
        return res.status(200).send({ message: "Load deleted successfully" });
      } else {
        console.log("No load with this id");
        return res.status(400).send({ message: "Error message" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async postLoadById(req, res) {
    try {
      const load = await Load.findOne({ _id: req.params.id });
      if (load.status === "NEW") {
        await Load.findOneAndUpdate(
          { _id: req.params.id },
          { status: "POSTED" }
        );
        load.logs.push({
          message: "This load is <POSTED>",
          time: new Date(),
        });
        const trucksIS = await Truck.find({
          status: "IS",
          assigned_to: { $ne: null },
        });
        if (trucksIS.length === 0) {
          await Load.findOneAndUpdate(
            { _id: req.params.id },
            { status: "NEW" }
          );
          load.logs.push({
            message:
              "No truck with assigned driver. Status rolled back to <NEW>",
            time: new Date(),
          });
          load.save();
          res
            .status(200)
            .send({
              message:
                "No truck with assigned driver. Status rolled back to <NEW>",
              driver_found: false,
            });
        } else {
          let bestTruck;
          let check = true;
          trucksIS.forEach((truck) => {
            const dimensions = truckTypes[truck.type].dimensions;
            const payload = truckTypes[truck.type].payload;
            if (
              check &&
              dimensions.width > load.dimensions.width &&
              dimensions.length > load.dimensions.length &&
              dimensions.height > load.dimensions.height &&
              payload > load.payload
            ) {
              bestTruck = truck;
              check === false;
            }
          });
          if (bestTruck) {
            await Truck.findOneAndUpdate(
              { _id: bestTruck._id },
              { status: "OL" }
            );
            await Load.findOneAndUpdate(
              { _id: req.params.id },
              {
                status: "ASSIGNED",
                state: "En route to Pick Up",
                assigned_to: bestTruck.assigned_to,
              }
            );
            load.logs.push({
              message: `Load assigned to driver with id ${bestTruck.assigned_to}`,
              time: new Date(),
            });
            res
              .status(200)
              .send({
                message: "Load posted successfully",
                driver_found: true,
              });
          } else {
            await Load.findOneAndUpdate(
              { _id: req.params.id },
              { status: "NEW" }
            );
            load.logs.push({
              message:
                "No truck with needed parametres. Status return to <NEW>",
              time: new Date(),
            });
            res
              .status(200)
              .send({
                message: "Load posted successfully",
                driver_found: false,
              });
          }
          load.save();
        }
      } else {
        res.status(400).send({ message: "Error message" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  async getLoadShippingInfoById(req, res) {
    try {
      const loadById = await Load.findOne({
        _id: req.params.id,
        status: "ASSIGNED",
      }).select("-__v");
      if (loadById) {
        await Truck.findOne({ assigned_to: loadById.assigned_to })
          .select("-__v")
          .then((truck) => {
            return res.status(200).send({ load: loadById, truck: truck });
          })
          .catch((error) => {
            return res.status(400).send({ message: "Error message" });
          });
      } else {
        console.log("No load with this id or this delivery finished");
        return res.status(400).send({ message: "Error message" });
      }
    } catch (error) {
      console.error(error);
      res.status(400).send({ message: "Error message" });
    }
  }

  
}

module.exports = new AuthController();
