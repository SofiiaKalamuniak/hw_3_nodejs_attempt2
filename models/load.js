const {Schema, model, } = require('mongoose');

const Load = new Schema({
  name: {type: String, required: true},
  payload: {type: Number, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  created_by: {type: Object, ref: 'User'},
  logs: {
    type: [{
      message: {type: String, required: true},
      time: {type: Date, required: true},
    }], default: [], _id: false,
  },
  assigned_to: {type: Object, ref: 'User'},
  status: {type: String, required: true, default: 'NEW'},
  state: {type: String, default: ''},
  dimensions: {
    width: {type: Number, required: true},
    length: {type: Number, required: true},
    height: {type: Number, required: true},
  },
  created_date: {type: Date, required: true},
});

module.exports = model('Load', Load);

