const {Schema, model, ObjectId} = require('mongoose');

const Truck = new Schema({
  created_by: {type: ObjectId, ref: 'User'},
  assigned_to: {type: ObjectId, ref: 'User'},
  status: {type: String, required: true, default: 'IS'},
  type: {type: String, required: true},
  created_date: {type: Date, required: true},
});

module.exports = model('Truck', Truck);

